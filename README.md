# Next Content-first vue template using Wordpress API

[![PSDS](http://psds.pl/wp/wp-content/uploads/2017/10/wordpress-vue.jpg)](http://psds.pl)

## Features:
- build using Vue.js
- using Wordpress API 
- clean, simple look
- just main elements was styled

## Instalation:
1. Clone repo where you want.
2. Run `npm install`.
3. Set your api adress in `src/config/routers.js`, e.g.
 ```
 export default {
  url: "WEBSITE URL (WITH HTTP)",
  posts: "/wp-json/wp/v2/posts",
  categories: "/wp-json/wp/v2/categories",
  tags: "/wp-json/wp/v2/tags",
  pages: "/wp-json/wp/v2/pages",
  comments: "/wp-json/wp/v2/comments",
  media: "/wp-json/wp/v2/media",
  menus: "/wp-json/menus/v1/menus" // it's plugin ;)
};
```
3. Run `npm run dev` or `npm run build` for production build.
