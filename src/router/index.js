import Vue from "vue";
import Router from "vue-router";
import Index from "@/components/index/Wrapper";
import Post from "@/components/post/Wrapper";
import Page from "@/components/page/Wrapper";
import Posts from "@/components/postList/Wrapper";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "index",
      component: Index
    },
    {
      path: "/post/:id",
      name: "post",
      component: Post
    },
    {
      path: "/page/:slug/:id",
      name: "page",
      component: Page,
      props: true
    },
    {
      path: "/posts",
      name: "posts",
      component: Posts,
      props: route => ({
        tags: route.query.tags
      })
    }
  ]
});
