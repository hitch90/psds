export default {
  url: "http://wp.psds.pl",
  posts: "/wp-json/wp/v2/posts",
  categories: "/wp-json/wp/v2/categories",
  tags: "/wp-json/wp/v2/tags",
  pages: "/wp-json/wp/v2/pages",
  comments: "/wp-json/wp/v2/comments",
  media: "/wp-json/wp/v2/media",
  menus: "/wp-json/menus/v1/menus"
};
