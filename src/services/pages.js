import axios from "axios";
import route from "@/config/routers";
export const getPages = async () => {
  try {
    const post = await axios.get(`${route.url}${route.pages}`);
    return post.data;
  } catch (err) {
    return err;
  }
};
export const getPage = async id => {
  try {
    const post = await axios.get(`${route.url}${route.pages}/${id}`);
    return post.data;
  } catch (err) {
    return err;
  }
};
