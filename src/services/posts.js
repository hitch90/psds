import axios from "axios";
import route from "@/config/routers";
export const getPost = async id => {
  try {
    const post = await axios.get(`${route.url}${route.posts}/${id}`);
    return post.data;
  } catch (err) {
    return err;
  }
};

export const getPosts = () => {
  // let queryUrl = "";
  // if (typeof params === "object") {
  //   for (let i in params) {
  //     queryUrl = queryUrl + i + "=" + params[i] + "&";
  //   }
  // }
  return axios
    .get(`${route.url}${route.posts}`)
    .then(response => {
      return response.data;
    })
    .catch(() => {
      return false;
    });
};

export const getMedia = id => {
  return axios
    .get(`${route.url}${route.media}/${id}`)
    .then(response => {
      return response.data;
    })
    .catch(() => {
      return false;
    });
};
