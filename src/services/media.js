import axios from "axios";
import route from "@/config/routers";

export const getMedia = id => {
  return axios
    .get(`${route.url}${route.media}/${id}`)
    .then(response => {
      return response.data;
    })
    .catch(() => {
      return false;
    });
};
