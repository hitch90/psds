import axios from "axios";
import route from "@/config/routers";

export const addComment = data => {
  return axios.post(`${route.url}${route.comments}`, data).then(response => {
    return response.data;
  });
};

export const getComments = (id = null) => {
  const query = id === null ? "?per_page=100" : `?post=${id}&per_page=100`;
  return axios.get(`${route.url}${route.comments}${query}`).then(response => {
    console.log(response);
    return response.data;
  });
};
