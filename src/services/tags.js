import axios from "axios";
import route from "@/config/routers";

export const getTags = (id = null) => {
  const url = id
    ? `${route.url}${route.tags}/${id}`
    : `${route.url}${route.tags}`;
  return axios
    .get(url)
    .then(response => {
      return response.data;
    })
    .catch(() => {
      return false;
    });
};
