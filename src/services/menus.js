import axios from "axios";
import route from "@/config/routers";
export const getMenus = async (id = null) => {
  try {
    const queryUrl = id
      ? `${route.url}${route.menus}/${id}`
      : `${route.url}${route.menus}`;
    const menu = await axios.get(queryUrl);
    return menu.data;
  } catch (err) {
    return err;
  }
};
