<template>
    <nav class="nav">
        <ul class="ul">
            <li v-if="menus.length && menus.items.length">
                <div :class="`hamburger ${menuVisible ? 'active' : ''}`" @click.prevent="showMenu">
                    <span class="hamburgerspan"></span>
                    <span class="hamburgerspan"></span>
                    <span class="hamburgerspan"></span>
                </div>
            </li>
            <li>
                <router-link :to="{ name: 'index'}">
                    Homepage
                </router-link>
            </li>
            <li v-for="page in pageList" :key="page.id">
                <router-link :to="{ name: 'page', params: { id: page.id, slug: page.slug }}">
                    {{ page.title.rendered }}
                </router-link>
            </li>
        </ul>
        <div :class="`main-menu ${menuVisible ? 'active' : ''}`" v-if="menus.length">
            <div class="main-menu-content">
                <menu-item v-for="menu in menus.items" :key="menu.id" :menu="menu" />

            </div>
        </div>
    </nav>
</template>

<script>
import { getPages } from '@/services/pages';
import { getMenus } from '@/services/menus';
import MenuItem from '@/components/common/MenuItem';

export default {
    name: 'app-nav',
    data() {
        return {
            pageList: [],
            menuVisible: false,
            menus: []
        }
    },
    components: {
        MenuItem
    },
    created() {
        this.getPages();
        this.getMenus();
    },
    methods: {
        getPages() {
            getPages().then((response) => {
                this.pageList = response;
            }).catch(() => {
                console.log(err);
            })
        },
        getMenus() {
            getMenus('top').then((response) => {
                this.menus = response;
            })
        },
        showMenu() {
            this.menuVisible = this.menuVisible ? false : true;
        }
    }
}
</script>

<style lang="scss" scoped>
.nav {
    text-align: center;
    margin: 30px 0 100px;
    position: relative;
    .ul {
        z-index: 12;
        background: #fff;
        max-width: 740px;
        margin: 0 auto;
        position: relative;
        display: grid;
        list-style: none;
        grid-auto-flow: column;
    }
    .hamburger {
        width: 30px;
        margin-top: 1px;
        cursor: pointer;
        &:hover,
        &.active {
            .hamburgerspan {
                &:nth-child(2) {
                    width: 30px;
                }
                &:nth-child(1) {
                    width: 20px;
                }
            }
        }
        .hamburgerspan {
            background: #42b983;
            border-radius: 3px;
            display: block;
            margin-bottom: 3px;
            transition: 0.3s all ease;
            height: 3px;
            width: 30px;
            &:nth-child(1) {
                width: 25px;
            }
            &:nth-child(2) {
                width: 20px;
            }
            &:nth-child(3) {
                width: 25px
            }
        }
    }
    .main-menu {
        height: 0;
        background: rgba(255, 255, 255, .95);
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: 10;
        overflow: hidden;
        transition: height .5s;
        display: flex;
        align-items: center;
        justify-content: center;
        &.active {
            height: 100vh;
        }
    }
}
</style>